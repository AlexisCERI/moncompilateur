# CERIcompiler

A simple compiler.
From : Pascal-like imperative LL(k) langage
To : 64 bit 80x86 assembly langage (AT&T)

J'ai rajouté à la fin de ce README quelques tests que j'ai effectué en plus, pour voir de nombreux cas de figures pour voir si la boucle for fonctionnait correctement.

**Download the repository :**

> git clone git@framagit.org:jourlin/cericompiler.git

**Build the compiler and test it :**

> make test

**Have a look at the output :**

> gedit test.s

**Debug the executable :**

> ddd ./test

**Commit the new version :**

> git commit -a -m "What's new..."

**Send to your framagit :**

> git push -u origin master

**Get from your framagit :**

> git pull -u origin master

**This version Can handle :**

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Identifier {"," Identifier} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Identifier ":=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}
// Identifier := Letter {(Letter|Digit)}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"

**Test2:** 

VAR		a,b,res :	INTEGER.

b:=10;
res:=0;
FOR a:=2 TO a<b DO
BEGIN
	DISPLAY 'r';
	DISPLAY 'e';
	DISPLAY 's';
	DISPLAY '=';
	res:=a*b;
	DISPLAY res;
	DISPLAY '\n'
END.

**Test3:**

VAR		a,b,res :	INTEGER.

b:=15;
res:=0;
FOR a:=0 TO a<b DO
BEGIN
	DISPLAY 'r';
	DISPLAY 'e';
	DISPLAY 's';
	DISPLAY '=';
	res:=a+b;
	DISPLAY res;
	DISPLAY '\n'
END.

**Test4: un for dans un for**

VAR		a,b,res :	INTEGER.

b:=15;
res:=0;
FOR a:=0 TO a<10 DO
	FOR b:=0 TO b<10 DO
BEGIN
		DISPLAY ('a');
		DISPLAY ('=');
		DISPLAY a;
		a:=a+1;
		DISPLAY ('\n');
		DISPLAY ('b');
		DISPLAY ('=');
		DISPLAY b;
		b:=b+1;
		DISPLAY ('\n')
END.



